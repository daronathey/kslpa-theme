<?php

$labels = array(
	'name'               => __( 'Upcoming Audits', 'spha' ),
	'singular_name'      => __( 'Upcoming Audit', 'spha' ),
	'add_new'            => _x( 'Add New Upcoming Audit', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Upcoming Audit', 'spha' ),
	'edit_item'          => __( 'Edit Upcoming Audit', 'spha' ),
	'new_item'           => __( 'New Upcoming Audit', 'spha' ),
	'view_item'          => __( 'View Upcoming Audit', 'spha' ),
	'search_items'       => __( 'Search Upcoming Audits', 'spha' ),
	'not_found'          => __( 'No Upcoming Audits found', 'spha' ),
	'not_found_in_trash' => __( 'No Upcoming Audits found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Upcoming Audit:', 'spha' ),
	'menu_name'          => __( 'Upcoming Audits', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-controls-skipforward',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
    'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'upcoming_audit', $args );