<?php

/**
 * Template Name: Upcoming Audits
 */

$archive_id = get_option('page_for_upcoming_audit');

$context = Timber::get_context();

$context['post'] = new TimberPost($archive_id);

$context['posts'] = Timber::get_posts();

Timber::render( 'upcoming-audits.twig', $context );